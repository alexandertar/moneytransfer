### Revolut Money Transfer

This is a simple web-service allowing basic account management and funds transfer.

Service runs on top of [Spark](http://sparkjava.com) framework.

Project is using Maven and was created in IntelliJ IDEA.

To start service simply run application as a standalone program. Web requests become available on [http://localhost:4567/]().

#### Supported requests

* Add new account (POST http://localhost:4567/accounts?currency='currency')
* Get account info (GET http://localhost:4567/accounts/'id')
* Delete account (DELETE http://localhost:4567/accounts/'id')
* Withdraw funds (PUT http://localhost:4567/accounts/'id'/withdraw?amount='amount')
* Deposit funds (PUT http://localhost:4567/accounts/'id'/deposit?amount='amount')
* Get all accounts (GET http://localhost:4567/accounts)
* Transfer funds (POST http://localhost:4567/accounts/'from-id'/'to-id'/transfer?currency='currency'&amount='amount')

Service supports automatic currency conversion in case funds are transferred between accounts with different currencies. Currency conversion is available for USD, EUR and GBP.
