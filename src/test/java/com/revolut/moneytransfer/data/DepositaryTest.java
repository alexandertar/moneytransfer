package com.revolut.moneytransfer.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepositaryTest {

    @Test
    void addAccountNew() {
        Depositary dep = new Depositary();
        Account acc = new Account("account1", "GBP");

        assertDoesNotThrow(() -> dep.addAccount(acc));
        assertDoesNotThrow(() -> dep.findAccount(acc.getId()));
    }

    @Test
    void addAccountExisting() {
        Depositary dep = new Depositary();
        Account acc1 = new Account("account1", "GBP");
        assertDoesNotThrow(() -> dep.addAccount(acc1));

        Account acc2 = new Account("account1", "USD");
        assertThrows(DataException.class, () -> dep.addAccount(acc2));
    }

    @Test
    void findAccountExists() throws DataException {
        Depositary dep = new Depositary();
        Account acc = new Account("account1", "GBP");
        assertDoesNotThrow(() -> dep.addAccount(acc));

        assertDoesNotThrow(() -> dep.findAccount(acc.getId()));
        assertEquals(acc, dep.findAccount(acc.getId()));
    }

    @Test
    void findAccountDoesNotExist() {
        Depositary dep = new Depositary();

        assertThrows(DataException.class, () -> dep.findAccount("account1"));
    }

    @Test
    void removeAccountExists() {
        Depositary dep = new Depositary();
        Account acc = new Account("account1", "GBP");
        assertDoesNotThrow(() -> dep.addAccount(acc));
        assertDoesNotThrow(() -> dep.findAccount(acc.getId()));

        assertDoesNotThrow(() -> dep.removeAccount(acc.getId()));

        assertThrows(DataException.class, () -> dep.findAccount(acc.getId()));
    }

    @Test
    void removeAccountDoesNotExist() {
        Depositary dep = new Depositary();

        assertThrows(DataException.class, () -> dep.removeAccount("account1"));
    }
}