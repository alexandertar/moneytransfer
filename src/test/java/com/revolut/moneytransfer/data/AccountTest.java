package com.revolut.moneytransfer.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void withdrawSuccess() {
        Account account = new Account("account1", "USD");
        // need to deposit first
        double balance = 200.0;
        account.deposit(balance);
        assertEquals(balance, account.getBalance());

        double amount = 100.0;
        assertDoesNotThrow(() -> account.withdraw(amount));
        assertEquals(balance - amount, account.getBalance(), 0.0001);
    }

    @Test
    void withdrawInsufficient() {
        Account account = new Account("account1", "USD");
        // need to deposit first
        double balance = 200.0;
        account.deposit(balance);
        assertEquals(balance, account.getBalance(), 0.0001);

        double amount = 300.0;
        assertThrows(AccountException.class, () -> account.withdraw(amount));
        assertEquals(balance, account.getBalance(), 0.0001);
    }

    @Test
    void deposit() {
        Account account = new Account("account1", "USD");
        assertEquals(0.0, account.getBalance(), 0.0001);

        double balance = 200.0;
        account.deposit(balance);
        assertEquals(balance, account.getBalance(), 0.0001);
    }
}