package com.revolut.moneytransfer.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyConverterTest {

    @Test
    void convert() throws CurrencyException {
        CurrencyConverter converter = new CurrencyConverter();

        assertDoesNotThrow(() -> converter.exchangeRate("EUR", "USD"));
        double rate = converter.exchangeRate("EUR", "USD");

        double amount = 100.0;
        assertDoesNotThrow(() -> converter.convert("EUR", "USD", amount));
        double converted = converter.convert("EUR", "USD", amount);
        assertEquals(amount * rate, converted, 0.0001);
    }

    @Test
    void convertNotSupported() {
        CurrencyConverter converter = new CurrencyConverter();

        double amount = 100.0;

        assertThrows(CurrencyException.class, () -> converter.convert("NZD", "USD", amount));
        assertThrows(CurrencyException.class, () -> converter.convert("NZD", "AUD", amount));
        assertThrows(CurrencyException.class, () -> converter.convert("USD", "AUD", amount));
    }

    @Test
    void exchangeRateSameCurrency() throws CurrencyException {
        CurrencyConverter converter = new CurrencyConverter();
        double rate = 1.0;

        assertDoesNotThrow(() -> converter.exchangeRate("USD", "USD"));
        assertEquals(rate, converter.exchangeRate("USD", "USD"), 0.0001);
        assertDoesNotThrow(() -> converter.exchangeRate("EUR", "EUR"));
        assertEquals(rate, converter.exchangeRate("EUR", "EUR"), 0.0001);
        assertDoesNotThrow(() -> converter.exchangeRate("GBP", "GBP"));
        assertEquals(rate, converter.exchangeRate("GBP", "GBP"), 0.0001);
    }

    @Test
    void exchangeRateNotSupported() {
        CurrencyConverter converter = new CurrencyConverter();

        assertThrows(CurrencyException.class, () -> converter.exchangeRate("NZD", "USD"));
        assertThrows(CurrencyException.class, () -> converter.exchangeRate("NZD", "AUD"));
        assertThrows(CurrencyException.class, () -> converter.exchangeRate("USD", "AUD"));
    }
}