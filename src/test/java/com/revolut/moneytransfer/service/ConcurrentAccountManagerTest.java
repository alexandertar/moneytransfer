package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.data.*;
import org.testng.annotations.*;

import java.util.List;
import java.util.Random;

import static org.testng.Assert.*;

public class ConcurrentAccountManagerTest {
    private Depositary depositary;
    private CurrencyConverter currencyConverter;
    private AccountManager accountManager;

    private Random randomNum = new Random();

    @BeforeClass
    public void beforeClass() throws DataException {
        depositary = new Depositary();
        currencyConverter = new CurrencyConverter();
        accountManager = new AccountManager(depositary, currencyConverter);

        Account acc1 = new Account("account1", "USD");
        Account acc2 = new Account("account2", "USD");
        Account acc3 = new Account("account3", "USD");
        Account acc4 = new Account("account4", "USD");
        Account acc5 = new Account("account5", "USD");
        depositary.addAccount(acc1);
        depositary.addAccount(acc2);
        depositary.addAccount(acc3);
        depositary.addAccount(acc4);
        depositary.addAccount(acc5);

        acc1.deposit(1000.0);
        acc2.deposit(1000.0);
        acc3.deposit(1000.0);
        acc4.deposit(1000.0);
        acc5.deposit(1000.0);
    }

    @Test(threadPoolSize = 100, invocationCount = 1000)
    public void testTransfer() throws CurrencyException {
        List<Account> accounts = accountManager.getAllAccounts();

        int randAcc1 = randomNum.nextInt(5);
        int randAcc2 = randomNum.nextInt(5);
        double randomAmount = 1000.0 * randomNum.nextDouble();
        TransferInfo tInfo = new TransferInfo(accounts.get(randAcc1), accounts.get(randAcc2), "USD", randomAmount);
        try {
            accountManager.transfer(tInfo);
        } catch (AccountException e) {
            // do nothing (skip the test)
        }
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        double sum = depositary.getAllAccounts().stream().map(Account::getBalance).reduce(0.0, (Double a, Double b) -> a + b);
        assertEquals(sum, 5000.0, 0.01);
    }
}