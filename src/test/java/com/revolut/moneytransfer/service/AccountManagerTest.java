package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.data.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountManagerTest {

    @Test
    void getAccountInfoExists() throws DataException {
        Depositary dep = new Depositary();
        Account acc = new Account("account1", "GBP");
        assertDoesNotThrow(() -> dep.addAccount(acc));

        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);
        assertDoesNotThrow(() -> accManager.getAccountInfo(acc.getId()));
        assertEquals(acc, accManager.getAccountInfo(acc.getId()));
    }

    @Test
    void getAccountInfoDoesNotExist() {
        Depositary dep = new Depositary();
        Account acc = new Account("account1", "GBP");
        assertDoesNotThrow(() -> dep.addAccount(acc));

        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);
        assertThrows(DataException.class, () -> accManager.getAccountInfo("account2"));
    }

    @Test
    void addAccount() throws DataException {
        Depositary dep = new Depositary();
        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);

        assertDoesNotThrow(() -> accManager.addAccount("account1", "USD"));

        assertDoesNotThrow(() -> accManager.getAccountInfo("account1"));
        Account acc = accManager.getAccountInfo("account1");
        assertEquals("account1", acc.getId());
        assertEquals("USD", acc.getCurrency());
    }

    @Test
    void addAccountNoCurrency() {
        Depositary dep = new Depositary();
        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);

        assertThrows(DataException.class, () -> accManager.addAccount("account1", null));
    }

    @Test
    void removeAccount() {
        Depositary dep = new Depositary();
        Account acc = new Account("account1", "GBP");
        assertDoesNotThrow(() -> dep.addAccount(acc));

        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);
        assertDoesNotThrow(() -> accManager.removeAccount(acc.getId()));

        assertThrows(DataException.class, () -> accManager.getAccountInfo(acc.getId()));
    }

    @Test
    void transferSuccessSameCurrency() {
        Depositary dep = new Depositary();
        Account acc1 = new Account("account1", "GBP");
        acc1.deposit(150.0);
        assertDoesNotThrow(() -> dep.addAccount(acc1));
        Account acc2 = new Account("account2", "GBP");
        acc2.deposit(250.0);
        assertDoesNotThrow(() -> dep.addAccount(acc2));

        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);

        double amount = 100.0;
        TransferInfo transferInfo = new TransferInfo(acc1, acc2, "GBP", amount);

        assertDoesNotThrow(() -> accManager.transfer(transferInfo));
        assertEquals(150.0 - amount, acc1.getBalance(), 0.0001);
        assertEquals(250.0 + amount, acc2.getBalance(), 0.0001);
    }

    @Test
    void transferSuccessDifferentCurrencies() throws CurrencyException {
        Depositary dep = new Depositary();
        Account acc1 = new Account("account1", "GBP");
        acc1.deposit(150.0);
        assertDoesNotThrow(() -> dep.addAccount(acc1));
        Account acc2 = new Account("account2", "USD");
        acc2.deposit(250.0);
        assertDoesNotThrow(() -> dep.addAccount(acc2));

        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);

        double amount = 100.0;
        TransferInfo transferInfo = new TransferInfo(acc1, acc2, "GBP", amount);
        double rate = converter.exchangeRate("GBP", "USD");

        assertDoesNotThrow(() -> accManager.transfer(transferInfo));
        assertEquals(150.0 - amount, acc1.getBalance(), 0.0001);
        assertEquals(250.0 + amount * rate, acc2.getBalance(), 1.0);
    }

    @Test
    void transferSuccessDifferentCurrenciesInverted() throws CurrencyException {
        Depositary dep = new Depositary();
        Account acc1 = new Account("account1", "GBP");
        acc1.deposit(150.0);
        assertDoesNotThrow(() -> dep.addAccount(acc1));
        Account acc2 = new Account("account2", "USD");
        acc2.deposit(250.0);
        assertDoesNotThrow(() -> dep.addAccount(acc2));

        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);

        double amount = 100.0;
        TransferInfo transferInfo = new TransferInfo(acc1, acc2, "USD", amount);
        double rate = converter.exchangeRate("USD", "GBP");

        assertDoesNotThrow(() -> accManager.transfer(transferInfo));
        assertEquals(150.0 - amount * rate, acc1.getBalance(), 1.0);
        assertEquals(250.0 + amount, acc2.getBalance(), 1.0);
    }

    @Test
    void transferFailNotSupportedCurrency() {
        Depositary dep = new Depositary();
        Account acc1 = new Account("account1", "GBP");
        acc1.deposit(150.0);
        assertDoesNotThrow(() -> dep.addAccount(acc1));
        Account acc2 = new Account("account2", "NZD");
        acc2.deposit(250.0);
        assertDoesNotThrow(() -> dep.addAccount(acc2));

        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);

        double amount = 100.0;
        TransferInfo transferInfo = new TransferInfo(acc1, acc2, "GBP", amount);

        assertThrows(CurrencyException.class, () -> accManager.transfer(transferInfo));
        assertEquals(150.0, acc1.getBalance());
        assertEquals(250.0, acc2.getBalance());
    }

    @Test
    void transferFailInsufficientFunds() {
        Depositary dep = new Depositary();
        Account acc1 = new Account("account1", "USD");
        acc1.deposit(100.0);
        assertDoesNotThrow(() -> dep.addAccount(acc1));
        Account acc2 = new Account("account2", "GBP");
        acc2.deposit(250.0);
        assertDoesNotThrow(() -> dep.addAccount(acc2));

        CurrencyConverter converter = new CurrencyConverter();

        AccountManager accManager = new AccountManager(dep, converter);

        double amount = 100.0;
        TransferInfo transferInfo = new TransferInfo(acc1, acc2, "GBP", amount);

        assertThrows(AccountException.class, () -> accManager.transfer(transferInfo));
        assertEquals(100.0, acc1.getBalance());
        assertEquals(250.0, acc2.getBalance());
    }
}