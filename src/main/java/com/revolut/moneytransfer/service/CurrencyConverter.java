package com.revolut.moneytransfer.service;

import java.util.HashMap;

import fj.P2;
import static fj.P.p;

// Mock class to provide some basic currency conversion functionality
// in reality it should call some other services to gather exchange rate
public class CurrencyConverter {
    private HashMap<P2<String, String>, Double> conversionMap = new HashMap<>();

    public CurrencyConverter() {
        conversionMap.put(p("GBP", "USD"), 1.34);
        conversionMap.put(p("GBP", "EUR"), 1.13);
        conversionMap.put(p("EUR", "USD"), 1.17);
        conversionMap.put(p("USD", "GBP"), 0.74);
        conversionMap.put(p("EUR", "GBP"), 0.87);
        conversionMap.put(p("USD", "EUR"), 0.84);
    }

    double convert(String from, String to, double amount) throws CurrencyException {
        return amount * exchangeRate(from, to);
    }

    double exchangeRate(String from, String to) throws CurrencyException {
        // trivial case
        if (from.equals(to)) {
            return 1.0;
        }

        P2<String, String> currPair = p(from, to);
        if (!conversionMap.containsKey(currPair)) {
            throw new CurrencyException("Currency pair conversion is not supported: " + from + " " + to);
        }
        return conversionMap.get(currPair);
    }
}
