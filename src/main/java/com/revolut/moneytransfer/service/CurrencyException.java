package com.revolut.moneytransfer.service;

public class CurrencyException extends Exception {
    public CurrencyException(String message) {
        super(message);
    }
}
