package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.data.*;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AccountManager {
    private Depositary depositary;
    private CurrencyConverter converter;
    private ReadWriteLock lock = new ReentrantReadWriteLock(true);

    public AccountManager(Depositary depositary, CurrencyConverter converter) {
        this.depositary = depositary;
        this.converter = converter;
    }

    public Account addAccount(String accountId, String currency) throws DataException {
        if (currency == null) {
            throw new DataException("Currency must be specified");
        }
        Account account = new Account(accountId, currency);
        lock.writeLock().lock();
        try {
            depositary.addAccount(account);
        } finally {
            lock.writeLock().unlock();
        }
        return account;
    }

    public void removeAccount(String accountId) throws DataException {
        lock.writeLock().lock();
        try {
            depositary.removeAccount(accountId);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Account accountWithdraw(String accountId, double amount) throws DataException, AccountException {
        Account account;
        lock.readLock().lock();
        try {
            account = depositary.findAccount(accountId);
            lock.writeLock().unlock();
            lock.writeLock().lock();
            account.withdraw(amount);
        } finally {
            lock.writeLock().unlock();
        }
        return account;
    }

    public Account accountDeposit(String accountId, double amount) throws DataException {
        Account account;
        lock.readLock().lock();
        try {
            account = depositary.findAccount(accountId);
            lock.writeLock().unlock();
            lock.writeLock().lock();
            account.deposit(amount);
        } finally {
            lock.writeLock().unlock();
        }
        return account;
    }

    public Account getAccountInfo(String accountId) throws DataException {
        Account account;
        lock.readLock().lock();
        try {
            account = depositary.findAccount(accountId);
        } finally {
            lock.readLock().unlock();
        }
        return account;
    }

    public List<Account> getAllAccounts() {
        lock.readLock().lock();
        List<Account> accounts = depositary.getAllAccounts();
        lock.readLock().unlock();
        return accounts;
    }

    public void transfer(TransferInfo transferInfo) throws AccountException, CurrencyException {
        Account from = transferInfo.getAccountFrom();
        Account to = transferInfo.getAccountTo();

        double amountToWithdraw;
        if (!transferInfo.getCurrency().equals(from.getCurrency())) {
            amountToWithdraw = converter.convert(transferInfo.getCurrency(), from.getCurrency(), transferInfo.getAmount());
        } else {
            amountToWithdraw = transferInfo.getAmount();
        }

        double amountToDeposit;
        if (!transferInfo.getCurrency().equals(to.getCurrency())) {
            amountToDeposit = converter.convert(from.getCurrency(), to.getCurrency(), amountToWithdraw);
        } else {
            amountToDeposit = transferInfo.getAmount();
        }

        try {
            lock.writeLock().lock();
            from.withdraw(amountToWithdraw);
            to.deposit(amountToDeposit);
        } finally {
            lock.writeLock().unlock();
        }
    }
}
