package com.revolut.moneytransfer.application;

import com.google.gson.Gson;
import spark.ResponseTransformer;

import fj.data.Either;
import fj.data.Option;

class JsonTransformer implements ResponseTransformer {

    private Gson gson = new Gson();

    @Override
    public String render(Object model) {
        if (model instanceof Either) {
            return renderEither((Either<Object, Object>) model);
        } else if (model instanceof Option) {
            return renderOption((Option<Object>) model);
        }
        return gson.toJson(model);
    }

    private String renderEither(Either<Object, Object> model) {
        if (model.isLeft()) {
            return gson.toJson(model.left().value());
        } else {
            return gson.toJson(model.right().value());
        }
    }

    private String renderOption(Option<Object> model) {
        if (model.isSome()) {
            return gson.toJson(model.some());
        } else {
            return gson.toJson(new Object());
        }
    }
}
