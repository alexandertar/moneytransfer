package com.revolut.moneytransfer.application;

import com.revolut.moneytransfer.data.*;
import com.revolut.moneytransfer.service.AccountManager;
import com.revolut.moneytransfer.service.CurrencyConverter;

import fj.data.Either;
import fj.data.Option;

import org.apache.commons.lang3.RandomStringUtils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import spark.Request;
import spark.Response;

import java.util.List;


class RequestProcessor {
    private static final Logger logger = LogManager.getLogger(RequestProcessor.class);

    private Depositary depositary = new Depositary();
    private CurrencyConverter converter = new CurrencyConverter();
    private AccountManager accountManager = new AccountManager(depositary, converter);

    Either<Account, ErrorMessage> addAccount(Request request, Response response) {
        String currency = request.queryParams("currency");
        String accountId = RandomStringUtils.randomAlphanumeric(10);

        try {
            Account account = accountManager.addAccount(accountId, currency);
            response.status(201); // 201 Created
            return Either.left(account);
        } catch (DataException e) {
            logger.error("Error adding account: " + e.getMessage());
            response.status(400);
            return Either.right(new ErrorMessage(e.getMessage()));
        }
    }

    Either<Account, ErrorMessage> getAccount(Request request, Response response) {
        String accountId = request.params(":id");
        try {
            Account account = accountManager.getAccountInfo(accountId);
            response.status(200);
            return Either.left(account);
        } catch (DataException e) {
            logger.error("Error getting account: " + e.getMessage());
            response.status(404);
            return Either.right(new ErrorMessage(e.getMessage()));
        }
    }

    Option<ErrorMessage> removeAccount(Request request, Response response) {
        String accountId = request.params(":id");
        try {
            accountManager.removeAccount(accountId);
            response.status(200);
            return Option.none();
        } catch (DataException e) {
            logger.error("Error removing account: " + e.getMessage());
            response.status(404);
            return Option.some(new ErrorMessage(e.getMessage()));
        }
    }

    List<Account> getAllAccounts() {
        return accountManager.getAllAccounts();
    }

    Either<Account, ErrorMessage> withdraw(Request request, Response response) {
        String accountId = request.params(":id");
        String amount = request.queryParams("amount");
        try {
            double amt = Double.parseDouble(amount);
            Account account = accountManager.accountWithdraw(accountId, amt);
            response.status(200);
            return Either.left(account);
        } catch (Exception e) {
            logger.error("Error withdrawing funds: " + e.getMessage());
            response.status(404);
            return Either.right(new ErrorMessage(e.getMessage()));
        }
    }

    Either<Account, ErrorMessage> deposit(Request request, Response response) {
        String accountId = request.params(":id");
        String amount = request.queryParams("amount");
        try {
            double amt = Double.parseDouble(amount);
            Account account = accountManager.accountDeposit(accountId, amt);
            response.status(200);
            return Either.left(account);
        } catch (Exception e) {
            logger.error("Error depositing funds: " + e.getMessage());
            response.status(404);
            return Either.right(new ErrorMessage(e.getMessage()));
        }
    }

    Either<TransferInfo, ErrorMessage> transfer(Request request, Response response) {
        String from = request.params(":from");
        String to = request.params(":to");
        String currency = request.queryParams("currency");
        String amount = request.queryParams("amount");

        try {
            double amt = Double.parseDouble(amount);
            Account accFrom = depositary.findAccount(from);
            Account accTo = depositary.findAccount(to);

            TransferInfo tInfo = new TransferInfo(accFrom, accTo, currency, amt);
            accountManager.transfer(tInfo);

            response.status(200);
            return Either.left(tInfo);
        } catch (Exception e) {
            logger.error("Error transferring funds: " + e.getMessage());
            response.status(400);
            return Either.right(new ErrorMessage(e.getMessage()));
        }
    }
}
