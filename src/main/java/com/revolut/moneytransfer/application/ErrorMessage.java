package com.revolut.moneytransfer.application;

class ErrorMessage {
    private String error;

    ErrorMessage(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
