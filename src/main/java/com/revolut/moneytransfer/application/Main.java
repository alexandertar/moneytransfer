package com.revolut.moneytransfer.application;

import com.revolut.moneytransfer.data.Account;
import fj.P2;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import static spark.Spark.*;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        RequestProcessor processor = new RequestProcessor();

        // Creates a new account, will return the ID to the created resource
        // currency is sent in the post body as x-www-urlencoded value e.g. currency=USD
        post("/accounts", "application/json", processor::addAccount, new JsonTransformer());

        // Gets account info by the provided id
        get("/accounts/:id", "application/json", processor::getAccount, new JsonTransformer());

        // Deletes account by the provided id
        delete("/accounts/:id", "application/json", processor::removeAccount, new JsonTransformer());

        // Withdraws funds from account by the provided id
        put("/accounts/:id/withdraw", "application/json", processor::withdraw, new JsonTransformer());

        // Deposits funds to account by the provided id
        put("/accounts/:id/deposit", "application/json", processor::deposit, new JsonTransformer());

        // Gets all accounts (ids)
        get("/accounts", "application/json", (request, response) -> processor.getAllAccounts(), new JsonTransformer());

        // Creates a new account, will return the ID to the created resource
        // currency is sent in the post body as x-www-urlencoded value e.g. currency=USD
        post("/accounts/:from/:to/transfer", "application/json", processor::transfer, new JsonTransformer());
    }
}
