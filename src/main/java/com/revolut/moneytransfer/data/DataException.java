package com.revolut.moneytransfer.data;

public class DataException extends Exception {
    public DataException(String message) {
        super(message);
    }
}
