package com.revolut.moneytransfer.data;

public class AccountException extends Exception {
    public AccountException(String message) {
        super(message);
    }
}
