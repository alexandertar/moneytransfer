package com.revolut.moneytransfer.data;

public class TransferInfo {
    private Account accountFrom;
    private Account accountTo;
    private String currency;
    private double amount;

    public TransferInfo(Account accountFrom, Account accountTo, String currency, double amount) {
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.currency = currency;
        this.amount = amount;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public Account getAccountTo() {
        return accountTo;
    }

    public double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }
}
