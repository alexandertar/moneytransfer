package com.revolut.moneytransfer.data;

public class Account {
    private String id;
    private String currency;
    private double balance = 0.0;

    public Account(String id, String currency) {
        this.id = id;
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public String getCurrency() {
        return currency;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double amount) throws AccountException {
        if (balance < amount) {
            throw new AccountException("Insufficient funds");
        }
        balance = balance - amount;
    }

    public void deposit(double amount) {
        balance = balance + amount;
    }
}
