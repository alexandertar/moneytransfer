package com.revolut.moneytransfer.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Depositary {
    private HashMap<String, Account> accounts = new HashMap<>();

    public void addAccount(Account account) throws DataException {
        if (accounts.containsKey(account.getId())) {
            throw new DataException("Account already exists");
        }
        accounts.put(account.getId(), account);
    }

    public Account findAccount(String accountId) throws DataException {
        if (!accounts.containsKey(accountId)) {
            throw new DataException("Account doesn't exist: " + accountId);
        }
        return accounts.get(accountId);
    }

    public void removeAccount(String accountId) throws DataException {
        if (!accounts.containsKey(accountId)) {
            throw new DataException("Account doesn't exist: " + accountId);
        }
        accounts.remove(accountId);
    }

    public List<Account> getAllAccounts() {
        return new ArrayList<>(accounts.values());
    }
}
